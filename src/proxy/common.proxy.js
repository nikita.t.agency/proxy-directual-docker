import "dotenv/config";
import express from "express";
import { createProxyMiddleware } from "http-proxy-middleware";

const commonProxyRouter = express.Router();

const commonProxy = createProxyMiddleware({
  target: process.env.API_HOST,
  changeOrigin: true,

  pathRewrite(pathReq, req) {
    let url = `${pathReq.split("?")[0]}?appID=${process.env.APP_ID}`;
    url = Object.entries(req.query).reduce(
      (newUrl, [key, value]) => `${newUrl}&${key}=${encodeURI(value)}`,
      url,
    );
    return url;
  },
  onProxyRes(proxyRes) {
    proxyRes.headers["x-added"] = "foobar"; // add new header to response
    delete proxyRes.headers["x-removed"]; // remove header from response
  },
});

commonProxyRouter.use("/api", (req, res) => {
  return commonProxy(req, res);
});

export default commonProxyRouter;
