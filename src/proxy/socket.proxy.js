import "dotenv/config";
import express from "express";
import { createProxyMiddleware } from "http-proxy-middleware";

const socketProxyRouter = express.Router();
const PATH = "/socket.io";

const socketProxy = createProxyMiddleware({
  target: process.env.API_HOST + PATH,
  changeOrigin: true,
  ws: true,
});

socketProxyRouter.use(PATH, (req, res) => {
  return socketProxy(req, res);
});

export default socketProxyRouter;
