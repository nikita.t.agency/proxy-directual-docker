import express from "express";
import appIdController from "./crud/controllers/appid.controller.js";
import commonProxyRouter from "./proxy/common.proxy.js";
import socketProxyRouter from "./proxy/socket.proxy.js";

export const app = express();
const port = 8080;

app.use((req, res, next) => {
  next();
});

app.use(commonProxyRouter);

app.use(socketProxyRouter);

app.use(appIdController);

app.listen(port, () => {
  console.log("Прокси запущено");
});
