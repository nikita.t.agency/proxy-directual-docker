import "dotenv/config";
import express from "express";

const appIdController = express.Router();
const APP_ID = process.env.SOCKET_APP_ID || "";

appIdController.get("/crud/appid", function (request, response, next) {
  return response.json({ APP_ID });
});

export default appIdController;
