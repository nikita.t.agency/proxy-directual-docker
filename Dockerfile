FROM node:20.11 as builder
# Create app directory
WORKDIR .
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
# Install npm
RUN npm install

# Bundle app source
COPY . .

CMD ["npm", "run", "start"]


